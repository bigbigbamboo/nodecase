const express=require('express')
const router = express.Router()

const stuFn = require('../control/index')


// connection.connect()
// connection.end()
// 查询数据
router.post('/',(req,res)=>{
  // console.log(req.body)
  stuFn.sqlFn('SELECT * FROM message',(data)=>{
        res.send(data)
      })
}),

// 增加数据
router.post('/add',(req,res)=>{
  // '孙宇','男',22,'basket boy'
  var sql = 'INSERT INTO message(name,sex,age,remarks) VALUES (?,?,?,?)'
  var sqlParams = [req.body.name,req.body.sex,req.body.age,req.body.remarks]
  stuFn.sqlQuertFn(sql,sqlParams,(data)=>{
        res.json({
          code:0,
          msg:'操作成功'
        })
      })
}),
// 修改数据
router.post('/update',(req,res)=>{
  // '孙宇','男',22,'basket boy'
  var sql = 'UPDATE message set name=?,sex=?,age=?,remarks=? where id=?'
  var sqlParams = [req.body.name,req.body.sex,req.body.age,req.body.remarks,req.body.id]
  stuFn.sqlQuertFn(sql,sqlParams,(data)=>{
        res.json({
          code:0,
          msg:'操作成功'
        })
      })
}),
// 删除数据
router.post('/delete',(req,res)=>{
  // '孙宇','男',22,'basket boy'
  var sql = 'DELETE FROM message where id=?'
  var sqlParams = [req.body.id]
  stuFn.sqlQuertFn(sql,sqlParams,(data)=>{
        res.json({
          code:0,
          msg:'操作成功'
        })
      })
}),

router.get('/',(req,res)=>{
    // console.log(req.query,'req.query')  获取入参
    res.send('get hello china')
}),



module.exports =router



