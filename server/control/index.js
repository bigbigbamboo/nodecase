var mysql = require('mysql')
var connection = mysql.createConnection({
  host: 'localhost',
  port:'3306',
  user: 'root',
  password: '19950507',
  database: 'user'
})


// 封装sqlFn不带占位符
function sqlFn(sql,callBack){
    connection.query(sql, (err, data)=> {
      if (err) return err
      callBack(data)
    })
  }
  // 封装sqlQuertFn带占位符
  function sqlQuertFn(sql,sqlParams,callBack){
    connection.query(sql, sqlParams,(err, data)=> {
      if (err) return err
      callBack(data)
    })
  }

  module.exports ={
    sqlFn,
    sqlQuertFn
  }